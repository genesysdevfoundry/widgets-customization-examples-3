# README #

Examples of customization with Genesys Widgets.

### Widgets Customization Examples III ###

*These examples, built using Genesys Widgets 9.0.003.03, present 3 different approaches to load Genesys Widgets:*
#### Using the All-In-One approach:
The full library (widgets.min.js) is loaded immediately.  
Two variations are provided: using <script> in the html page to include the library, or using windows.onload method.

#### Using Lazy-Loading approach:
CXBus is loaded immediately (cxbus.min.js) - the different widgets/plugins depending on use (pre-load or on first request).

Each sample also includes an example of Custom Extension/Plugin definition (opening of a Toaster):

* With All-In-One approach, the custom plugin (TestExtension) is defined in widget_extension.js.

* With Lazy-Loading approach, the custom plugin (TestExtension) is defined in lib\plugins\testextension.min.js.
	
Note that Genesys Widgets libraries and css are not provided in this repository, and will have to be installed separately.

### How do I get set up? ###

#### Summary of set up

Get the source for the different examples in this repository.

Download Genesys Widgets 9.0.003.03 and copy:

* widgets.min.css into the \stylesheets directory

* widgets.min.js into the \lib directory (for All-In-One approach)

* cxbus.min.js into the \lib directory, and the different plugins into the lib\plugins directory (for Lazy-Loading approach).

Genesys Widgets libraries and css are not provided in this repository.

#### Configuration

At the top of the widget_config.js file, change the service URLs defined in the gmsServicesConfig object according to your environment.

The WebChat, SideBar, Channel Selector and TestExtension widgets are configured and enabled in the different samples.

Note that even if you don’t have a valid URL for Chat Service – ex: service not configured in GMS, in your environment - you can still play with the widgets (open, close, prefill form, …).


### Available Customization Examples III ###

* Sample using All-In-One approach (window.onload method)

* Sample using All-In-One approach (script include method)

* Sample using Lazy-Loading approach


