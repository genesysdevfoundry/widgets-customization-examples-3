/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples III - Sample using All-In-One approach (window.onload method)
 * 
 * Base code and page,
 * With Sidebar, Channel Selector, WebChat, SendMessage, Callback and TestExtension widgets configured
 * Providing buttons to open WebChat, SendMessage, Callback, ChannelSelector and TestExtension
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initWidgetExtension();

// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (QuickBus) {

    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};

// ##### Import Widget Script and CSS (Widget Core, Widget Extension, Custom Override)

(function (o) {
    var f = function () {
        var d = o.location;
        o.aTags = o.aTags || [];
        for (var i = 0; i < o.aTags.length; i++) {
            var oTag = o.aTags[i];
            var fs = d.getElementsByTagName(oTag.type)[0], e;
            if (d.getElementById(oTag.id))
                return;
            e = d.createElement(oTag.type);
            e.id = oTag.id;
            if (oTag.type == "script") {
                e.src = oTag.path;
            } else {
                e.type = 'text/css';
                e.rel = 'stylesheet';
                e.href = oTag.path;
            }
            if (fs) {
                fs.parentNode.insertBefore(e, fs);
            } else {
                d.head.appendChild(e);
            }
        }
    }, ol = window.onload;
    if (o.onload) {
        typeof window.onload != "function" ? window.onload = f : window.onload = function () {
            ol();
            f();
        }
    } else
        f();
})({

    location: document,
    onload: false,
    aTags: [

        {
            type: "script",
            id: "genesys-cx-widget-script",
            path: "lib/widgets.min.js"
        },
        {
            type: "link",
            id: "genesys-cx-widget-styles",
            path: "stylesheets/widgets.min.css"
        }

    ]
});




