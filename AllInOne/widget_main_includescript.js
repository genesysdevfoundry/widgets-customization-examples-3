/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples III - Sample using All-In-One approach (script include method)
 * 
 * Base code and page,
 * With Sidebar, Channel Selector, WebChat, SendMessage, Callback and TestExtension widgets configured
 * Providing buttons to open WebChat, SendMessage, Callback, ChannelSelector and TestExtension
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Widget Configuration

initWidgetExtension();

// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (QuickBus) {

    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');
};




