var initWidgetExtension = function () {

    if (!window._genesys)
        window._genesys = {};

    if (!window._gt)
        window._gt = [];

    if (!window._genesys.widgets)
        window._genesys.widgets = {};

    if (!window._genesys.widgets.extensions) {
        window._genesys.widgets.extensions = {};
    }

    window._genesys.widgets.extensions["TestExtension"] = function ($, CXBus, Common) {

		// Create your plugin interface on CXBus
		// Your plugin's name must match the filename. 
		// The filename must be lowercase, your plugin name can be any case
        var oTestExtension = CXBus.registerPlugin("TestExtension");

        oTestExtension.registerEvents(["ready", "opened", "closed"]);

        oTestExtension.subscribe("TestExtension.ready", function (e) { console.log("INFO: TestExtension.ready " + JSON.stringify(e)) });
        oTestExtension.subscribe("TestExtension.opened", function (e) { console.log("INFO: TestExtension.opened " + JSON.stringify(e)) });
        oTestExtension.subscribe("TestExtension.closed", function (e) { console.log("INFO: TestExtension.closed " + JSON.stringify(e)) });

        oTestExtension.registerCommand("testCommand", function(e){

			console.log("testCommand fired", e);

			console.log("options passed to command", e.data);

			// Commands are asynchronous and return promises
			// You must always resolve or reject the deferred method here
			e.deferred.resolve({some: "data", if: "any", to: "return"}); 

			// e.deferred.reject("some error message to show in the logs");
		});

        oTestExtension.registerCommand("open", function (e) {
            console.log("TestExtension.open executed");

            oTestExtension.command("Toaster.open",
                {
                    type: 'generic',
                    title: 'Test Extension Title',
                    body: 'Test Extension Body',
                    icon: 'videochat',
                    controls: 'close',
                    immutable: false,
                    buttons: {
                        type: 'binary',
                        primary: 'Accept',
                        secondary: 'Decline'
                    }
                }
            ).done(function (e2) {
                // Toaster opened successfully
                $(e2.html).find(".cx-btn.cx-btn-default").click(function () {
                    alert("Decline has been selected");
                    oTestExtension.command('Toaster.close');
                });

                $(e2.html).find(".cx-btn.cx-btn-primary").click(function () {
                    alert("Accept has been selected");
                    oTestExtension.command('Toaster.close');
                });

                oTestExtension.subscribe("Toaster.closed", function (e) {
                    oTestExtension.unsubscribe("Toaster.closed");
                    oTestExtension.publish("closed");
                });

                oTestExtension.publish("opened");
            }).fail(function (e3) {
                // Toaster failed to open properly
            });

            e.deferred.resolve(); // or e.deferred.reject(); if the command cannot complete
        });

		// For one-time only events. When plugins subscribe to this event it will automatically be published again. 
		// Avoid using this for events other than "ready".
		oTestExtension.republish("ready"); 

		// Must let CXBus know your widget is ready to execute commands
		// With lazy-loading, command requests are buffered until the required file is loaded
		// This buffer is executed once your plugin marks itself "ready" with this method.
		oTestExtension.ready(); 

    };

}

